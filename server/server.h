
#include <stdbool.h>
#include "../server_client_communication.h"

#ifndef __SERVER_H__
#define __SERVER_H__

#define EMPTY_ID -1
#define EMPTY_CONNECTION -1
#define MAX_CONNECTIONS 3
#define ID 1

struct malware_client {
    int id;
    int fd_read;
    int fd_write;
};

void initialize_server(int port);
int add_connection(int fd_read, int fd_write);
void reset_connection(struct malware_client * cli);
void reset_all_connections();
void shutdown_server();
void on_packet_received(int socket, struct mpacket * packet);
void * create_handshake(void* ptr_client_fd);
void init_connections();
int get_read_socket_from_id();
void * write_all_from_terminal();
void print_connections();

#endif // __SERVER_H__