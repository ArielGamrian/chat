
#include "server.h"
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <limits.h>

bool is_number(char * arg);

int main (int argc, char ** argv) {

    system("clear");

    signal(SIGTSTP, shutdown_server);
    signal(SIGINT, shutdown_server);
    signal(SIGQUIT, shutdown_server);

    // if (argc < 2) {
    //     printf("Please enter port number\n");
    //     exit(1);
    // }

    // char * name = (char *) argv[0];

    // printf("Creating a server - '%s'\n", name);

    // int port = strtol(argv[1], NULL, 10);
    // if (LONG_MAX == port || LONG_MIN == port) {
    //     printf("The port should be a positive number!\n");
    //     exit(1);
    // }

    initialize_server(8080);
}