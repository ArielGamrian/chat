
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "server.h"


struct malware_client * clients_connected[MAX_CONNECTIONS];
int listening_socket;
int id_counter = 101;

// initializing the server
void initialize_server(int port) {

    init_connections();

    printf("Initializing server on port %d\n", port);

    int client_fd;
    struct sockaddr_in s_address;
    int address_len = sizeof(s_address);

    listening_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (-1 == listening_socket) {
        printf("Error creating a socket. errno = %d\n", errno);
        exit(EXIT_FAILURE);
    }

    s_address.sin_port = htons(port);
    s_address.sin_family = AF_INET;
    s_address.sin_addr.s_addr = INADDR_ANY;

    if (-1 == bind(listening_socket, (struct sockaddr*) &s_address, address_len)) {
        printf("Error binding the address to the socket. errno = %d\n", errno);
        exit(EXIT_FAILURE);
    }

    if (-1 == listen(listening_socket, MAX_CONNECTIONS)) {
        printf("Error trying to listen to fd. errno = %d\n", errno);
        exit(EXIT_FAILURE);
    }

    printf("Listening on port %d...\n", port);
    
    while (1) {
        client_fd = accept(listening_socket, (struct sockaddr *) &s_address, (socklen_t*) &address_len);
        if (-1 == client_fd) {
            printf("Error accepting a connection. errno = %d\n", errno);
            exit(EXIT_FAILURE);
        }
        
        char * client_ip = inet_ntoa(s_address.sin_addr);
        int client_port = (int) ntohs(s_address.sin_port);

        printf("--------------------\n");
        printf("Client connected:\nip = %s\n", client_ip);
        printf("port = %d\n", client_port);
        printf("fd = %d\n", client_fd);
        printf("--------------------\n");

        pthread_t t;
        // thread for each socket
        pthread_create(&t, NULL, create_handshake, (void *) &client_fd);
    }
}

void add_read_connection(int id, int fd_read) {
    for (int i = 0; i < MAX_CONNECTIONS; i++) {
        if (clients_connected[i]->id == id) {
            clients_connected[i]->fd_read = fd_read;
            return;
        }
    }
    printf("On add_write_connection(): No id %d found\n", id);
}

void * create_handshake(void* ptr_client_fd) {

    int socket = *((int *) ptr_client_fd);

    read_from_socket(socket, false, &on_packet_received);

    pthread_t t;
    pthread_create(&t, NULL, write_all_from_terminal, NULL);
    pthread_join(t, NULL);
    return NULL;
}

void on_packet_received(int socket, struct mpacket * packet) {

    switch (packet->packet_type)
    {
    case PACKET_TYPE_HANDSHAKE:

        if (packet->id_of_sender == NO_ID) {
            int id = add_connection(EMPTY_CONNECTION, socket);
            

            int socket = get_read_socket_from_id(id);
            send_packet(socket, ID, PACKET_TYPE_HANDSHAKE, sizeof(id), (int *) &id);
        }
        else {
            add_read_connection(packet->id_of_sender, socket);
            read_from_socket(socket, true, &on_packet_received);
        }
        
        break;

    case PACKET_TYPE_MESSAGE:
        //printf("Recived from client: %s\n", packet->value);
        break;

    default:
        printf("Wrong packet type!\n");
        break;
    }

    free(packet);
    
    //print_connections();
}

void print_connections() {

    printf("Printing connections:\n");
    for (int i = 0; i < MAX_CONNECTIONS; i++) {
        if (clients_connected[i]->id == EMPTY_ID) {
            continue;
        }

        printf("Id = %d | read = %d | write = %d\n", 
        clients_connected[i]->id, clients_connected[i]->fd_read, clients_connected[i]->fd_write);
    }
}

void * write_all_from_terminal () {

    char buffer[0];

    while (1) {
        scanf("%s", buffer);
        for (int i = 0; i < MAX_CONNECTIONS; i++) {
            if (clients_connected[i]->id == NO_ID) {
                continue;
            }
            send_packet(clients_connected[i]->fd_write, ID, PACKET_TYPE_MESSAGE, strlen(buffer) + 1, buffer);
        }
    }
    return NULL;
}

int get_read_socket_from_id(int id) {
    for (int i = 0; i < MAX_CONNECTIONS; i++) {
        if (clients_connected[i]->id == id) {
            return clients_connected[i]->fd_write;
        }
    }
    printf("on get_read_socket_from_id(): No id '%d' found\n", id);
    return NO_ID;
}

// add connection to connections array. returns client id.
int add_connection(int fd_read, int fd_write) {

    for (int i = 0; i < MAX_CONNECTIONS; i++) {
        if (clients_connected[i]->id == EMPTY_ID) {
            clients_connected[i]->id = id_counter++;
            clients_connected[i]->fd_read = fd_read;
            clients_connected[i]->fd_write = fd_write;

            return clients_connected[i]->id;
        }
    }
    printf("On 'add_connection()': Reached connections limit!\n");
    return NO_ID;
}

// id = NULL to remove all
void reset_connection(struct malware_client * cli) {
    
    if (cli->fd_read != EMPTY_CONNECTION) {
        close(cli->fd_read);
        cli->fd_read = EMPTY_CONNECTION;
    }

    if (cli->fd_write != EMPTY_CONNECTION) {
        close(cli->fd_write);
        cli->fd_write = EMPTY_CONNECTION;
    }
    cli->id = EMPTY_ID;

    //printf("On 'reset_connection()': No conenction found.. id = %d\n", id);
}

void init_connections() {
    for (int i = 0; i < MAX_CONNECTIONS; i++) {

        //struct client c = {.id = EMPTY_ID, .fd_read = EMPTY_CONNECTION, .fd_write = EMPTY_CONNECTION};

        clients_connected[i] = malloc(sizeof(struct malware_client));

        clients_connected[i]->fd_read = EMPTY_CONNECTION;
        clients_connected[i]->fd_write = EMPTY_CONNECTION;
        clients_connected[i]->id = EMPTY_ID;
    }
}

void reset_all_connections() {
    for (int i = 0; i < MAX_CONNECTIONS; i++) {
        reset_connection(clients_connected[i]);
    }
}

void shutdown_server() {
    printf("Shutting server down...\n");
    reset_all_connections();
    close(listening_socket);
}