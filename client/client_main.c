
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <signal.h>
#include "client.h"

int main(int argc, char ** argv) {

    system("clear");

    signal(SIGTSTP, shutdown_client);
    signal(SIGINT, shutdown_client);
    signal(SIGQUIT, shutdown_client);


    // if (argc < 3) {
    //     printf("Please enter port number\n");
    //     exit(1);
    // }

    // char * name = (char *) argv[0];

    // printf("Creating a client - '%s'\n", name);

    // int port = strtol(argv[1], NULL, 10);
    // if (LONG_MAX == port || LONG_MIN == port) {
    //     printf("The port should be a positive number!\n");
    //     exit(1);
    // }

    initialize_client(NULL, 0);
}

// TODO: insert check to the address