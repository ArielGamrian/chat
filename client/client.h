
#ifndef __CLIENT_H__
#define __CLIENT_H__
#include "../server_client_communication.h"

void initialize_client(char * address, int port);
void shutdown_client();
void connect_to_server(int * fd);
void create_handshake();
void * __read_from_socket(void* ptr_client_fd);
void on_packet_received(int socket, struct mpacket * packet);
#endif // __CLIENT_H__