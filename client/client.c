
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <signal.h>
#include "client.h"

int read_socket, write_socket, id = NO_ID;
pthread_t t_read, t_write;
int port = 8080;
struct sockaddr_in s_address;
int address_len = sizeof(s_address);

void * __read_from_socket(void* ptr_client_fd);
void * __write_to_socket(void* ptr_client_fd);
void connect_to_server(int * fd);
void create_handshake();

void initialize_client(char * address, int p) {

    printf("Initializing client...\n");

    connect_to_server(&read_socket);

    create_handshake();

    printf("Connected successfully to server :)\n");

    pthread_create(&t_write, NULL, __write_to_socket, (void *) &write_socket);
    pthread_create(&t_read, NULL, __read_from_socket, (void *) &read_socket);

    pthread_join(t_read, NULL);
}

void connect_to_server(int * fd) {
    *fd = socket(AF_INET, SOCK_STREAM, 0);

    if (-1 == *fd) {
        printf("Error creating a socket. errno = %d\n", errno);
        exit(EXIT_FAILURE);
    }

    s_address.sin_port = htons(port);
    s_address.sin_family = AF_INET;
    s_address.sin_addr.s_addr = INADDR_ANY;

    if (-1 == connect(*fd, (struct sockaddr *) &s_address, address_len)) {
        printf("Error while connecting to server. errno = %d\n", errno);
        exit(EXIT_FAILURE);
    }
}

void create_handshake() {
    char * str = "test";
    uint8_t length = sizeof(str);
    send_packet(read_socket, id, PACKET_TYPE_HANDSHAKE, length, str);

    read_from_socket(read_socket, false, &on_packet_received);
}

void * __read_from_socket(void* ptr_client_fd) {
    int socket = *((int *) ptr_client_fd);

    read_from_socket(socket, true, &on_packet_received);
    return NULL;
}

void on_packet_received(int socket, struct mpacket * packet) {

    switch (packet->packet_type)
    {
    case PACKET_TYPE_HANDSHAKE:

        id = *(int *) packet->value;

        connect_to_server(&write_socket);

        send_packet(write_socket, *(int*) packet->value, PACKET_TYPE_HANDSHAKE, 0, NULL);

        break;

    case PACKET_TYPE_MESSAGE:
        //printf("Recived from server: %s\n", packet->value);
        break;

    default:
        printf("Wrong packet type!\n");
        break;
    }

    free(packet);
}

void * __write_to_socket(void* ptr_client_fd) {
    int socket = *((int *) ptr_client_fd);
    write_to_socket_from_terminal(socket, id);

    return NULL;
}

void shutdown_client() {
    printf("Shutting down client...\n");
    pthread_cancel(t_read);
    pthread_cancel(t_write);
    close(read_socket);
    close(write_socket);
    exit(0);
}