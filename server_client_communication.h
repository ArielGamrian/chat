
#ifndef __SERVER_CLIENT_COMMUNICATION_H__
#define __SERVER_CLIENT_COMMUNICATION_H__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <ctype.h>
#include <time.h>

#define PACKET_MAX_SIZE 255

typedef __uint8_t PACKET_TYPE;
typedef __uint8_t SOCKET_TYPE;

#define PACKET_TYPE_HANDSHAKE 1
#define PACKET_TYPE_MESSAGE 2
#define SOCKET_TYPE_READ 1
#define SOCKET_TYPE_WRITE 2
#define NO_ID -1

struct mpacket {
    int id_of_sender;
    PACKET_TYPE packet_type;
    __uint8_t length_of_value;
    char value[0];
};

void send_packet(int socket_to_send, int sender_id, __uint8_t type, __uint8_t length_of_value, void* value);
void read_from_socket(int socket_to_read, bool permenant, void(*on_packet_received) (int, struct mpacket*));
void write_to_socket_from_terminal(int socket_to_send, int sender_id);
void print_packet(char str[], struct mpacket * packet);

#endif //__SERVER_CLIENT_COMMUNICATION_H__