
#include "server_client_communication.h"

bool is_empty(char s[]);

void send_packet(int socket_to_send, int sender_id, __uint8_t type, __uint8_t length_of_value, void* value) {

    int buffer_size = sizeof(struct mpacket) + length_of_value;
    char buffer[buffer_size];
    int i = 0;

    memcpy(&buffer[i], &sender_id, sizeof(sender_id));
    i+=sizeof(sender_id);

    buffer[i] = type;
    i += sizeof(type);

    buffer[i] = length_of_value;
    i += sizeof(length_of_value);

    memcpy(&buffer[i], value, length_of_value);

    struct mpacket * p = (struct mpacket*) buffer;
    print_packet("SEND", p);

    write(socket_to_send, buffer, buffer_size);
}

void read_from_socket(int socket, bool permenant, void(*on_packet_received) (int, struct mpacket*)) {
    
    struct mpacket * packet;
    int bytes_read;
    char read_buffer[PACKET_MAX_SIZE];
    do {
        
        bzero(read_buffer, PACKET_MAX_SIZE);

        bytes_read = read(socket, (char*) read_buffer, PACKET_MAX_SIZE);
        
        if (0 == bytes_read || is_empty(read_buffer) || strlen(read_buffer) == 0) {
            continue;
        }

        if (-1 == bytes_read) {
            printf("Error while reading data. errno = %d\n", errno);
        }
        
        __uint8_t packet_length = read_buffer[5];
        int packet_size = sizeof(packet) + packet_length;
        packet = malloc(packet_size);
        memcpy(packet, read_buffer, packet_size);

        print_packet("READ", packet);

        (*on_packet_received)(socket, packet);  

    } while (permenant);
}

bool is_empty(char s[]) {
  for (int i = 0; i < PACKET_MAX_SIZE; i++) {
      if (s[i] == '\0')
        return true;
      if (!isspace(s[i])) {
          return false;
      }
  }
  return true;
}

void write_to_socket_from_terminal(int socket_to_send, int sender_id) {

    char buffer[0];

    while (1) {
        scanf("%s", buffer);
        send_packet(socket_to_send, sender_id, PACKET_TYPE_MESSAGE, strlen(buffer) + 1, buffer);
    }
}

void print_packet(char str[], struct mpacket * packet) {
    printf("%s: id = %d|type = %d|length = %d|value = %s\n", 
    str, packet->id_of_sender, packet->packet_type, packet->length_of_value, packet->value);
}
